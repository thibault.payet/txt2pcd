#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <ios>
#include <iostream>
#include <string>

#include <pcl/io/ascii_io.h>
#include <pcl/io/pcd_io.h>

void txt2pcd( std::filesystem::path input, std::filesystem::path output )
{
    std::ifstream inputFile{ input.c_str() };

    if ( inputFile )
    {
        pcl::PointCloud<pcl::PointXYZRGB> cloud;

        // Read header
        std::string line;
        auto len = inputFile.tellg();

        char buf{ '\0' };

        do
        {
            inputFile.get( buf );
            if ( buf != '\0' && buf != '\n' )
            {
                line.push_back( buf );
            }
        } while ( buf != '\0' && buf != '\n' );

        auto numberOfSpace = std::count( std::begin( line ), std::end( line ), ' ' );

        if ( line.back() == ' ' )
        {
            if ( numberOfSpace > 0 )
            {
                --numberOfSpace;
            }
        }

        auto const numArgument = numberOfSpace + 1;

        inputFile.seekg( len, std::ios_base::beg );

        pcl::PCDWriter pcdWriter{};
        if ( numArgument == 6 )
        {
            while ( !inputFile.eof() )
            {
                pcl::PointXYZRGB point;

                std::string buf;

                inputFile >> buf;
                point.x = std::atof( buf.c_str() );
                buf.clear();

                inputFile >> buf;
                point.y = std::atof( buf.c_str() );
                buf.clear();

                inputFile >> buf;
                point.z = std::atof( buf.c_str() );
                buf.clear();

                int r, g, b;

                inputFile >> buf;
                r = std::atoi( buf.c_str() );
                buf.clear();
                inputFile >> buf;
                g = std::atoi( buf.c_str() );
                buf.clear();
                inputFile >> buf;
                b = std::atoi( buf.c_str() );
                buf.clear();

                point.r = static_cast<char>( r );
                point.g = static_cast<char>( g );
                point.b = static_cast<char>( b );

                cloud.push_back( point );
            }
        }
        else
        {
            inputFile.close();
            pcl::ASCIIReader reader;

            auto ext = input.extension().string();

            reader.setExtension( ext );


            reader.read( input.string(), cloud );
        }

        pcdWriter.writeBinaryCompressed( output.string(), cloud );
    }
}

void printHelp()
{
    std::cout << "Usage: txt2pcd inputCloud.txt [outputCloud.pcd]\n";
}

int main( int argc, char** argv )
{
    if ( argc == 3 )
    {
        try
        {
            txt2pcd( argv[ 1 ], argv[ 2 ] );
        }
        catch ( std::exception const& e )
        {
            std::cerr << e.what() << "\n";
        }
    }
    else if ( argc == 2 )
    {
        auto basefilename = std::filesystem::path{ argv[ 1 ] };
        auto outfilename  = basefilename;
        outfilename.replace_extension( std::filesystem::path{ "pcd" } );

        txt2pcd( basefilename, outfilename );
    }
    else
    {
        printHelp();
    }

    return 0;
}
